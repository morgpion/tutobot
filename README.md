# tutoBot

## Description
Bot Discord commencé avec le tuto de Grafikart, amélioré pour la v1 avec 2 foncitonnalités :
*  une commande t!prez de présentation du bot
*  une commande t!help de première version d'aide.

En cours d'amélioration pour lister les fichiers musicaux à lire, et chercher à lister les commandes disponibles automatiquement.


## Modules

### Discord.js
Node_module embarquant des classes et méthodes pour interagir avec l'API de discord
**Installation avec npm** npm install --save discord.js


### FFmpeg
[Doc de FFmpeg](https://www.ffmpeg.org/)


### Dotenv 
**fichier .env** BOT_TOKEN=LeTokenEntierSansGuillemets

**Si config via la CLI**

index.js :
`require('dotenv').config;`

Lancer le script avec : 
`node -r dotenv/config index.js`

**Si config dans le code**
index.js :
`const dotenv = require('dotenv');`
`const result = dotenv.config();`

Lancer le script avec : `node index.js`




