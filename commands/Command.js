
//  classe Command qui structure un squelette de commande
//  déclaration et export de la classe
module.exports = class Command {
//  méthodes statiques = accessibles hors de la classe, nécessaire pour reconnaître 
//  qu'une commande est écrite dans un message

  //  parser le message envoyé sur le channel pour trouver si c'est 
  //  une commande et laquelle
  static parse(message) {
    //  modifier le but de cette commande, on ne veut pas l'id mais le pseudo
    //  si ce n'est pas un bot qui envoie une commande, afficher son id
    // if (!message.author.bot) message.channel.send(message.author.id);

    //  si le nom d'une commande est reconnu, on lance une action
    if(this.match(message)){
      this.action(message);
      return true;
    }
    return false;
  }

  //  ne pas activer le contenu des messages (ne pas suivre les liens web par exemple)
  static match(message) {
    return false;
  }

  //  méthode pour envoyer un message, utilisée dans la méthode parse(message)
  static action(message) {

  }

}
