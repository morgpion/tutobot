
//  import de la classe Command pour l'utiliser
const Command = require('./Command');

//  déclaration de la classe Prez, qui hérite de la classe Command
module.exports = class Prez extends Command {
//  méthodes héritées de Command

  //  implémentation de la commande pour appeler cette fonctionnalité
  static match(message) {
    //  pour vérifier l'ordre du parcours des classes, 
    //  peut servir à optimiser l'ordre sur index.js
    console.log('check prez');
    //  retourne le nom de la commande : méthode match('t!prez')
    return message.content.startsWith('t!prez')
  }

  //  implémentation de la méthode action(message)
  static action (message) {
    let rep_prez = `Merci de t'intéresser à moi ! Je m'appelle TutoBot et j'ai été créé par morgpion.\n`;
    rep_prez += `Pour obtenir la liste de mes commandes, tape "t!help"`
    message.channel.send(rep_prez);
  }

}

