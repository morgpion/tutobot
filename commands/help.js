
//  import de la classe Command pour l'utiliser
const Command = require('./Command');

//  déclaration de la classe Help, qui hérite de la classe Command
module.exports = class Help extends Command {
//  méthodes héritées de Command

  //  implémentation de la commande pour appeler cette fonctionnalité
  static match(message) {
    //  pour vérifier l'ordre du parcours des classes, 
    //  peut servir à optimiser l'ordre sur index.js
    console.log('check help');
    //  retourne le nom de la commande : méthode match('t!google')
    return message.content.startsWith('t!help')
  }

  //  implémentation de la méthode action(message)
  static action (message) {
    let rep_help = `Mes commandes sont préfixées avec "t!" (dit "ti"), et leur liste est :\n`;
    rep_help += `t!help\tt!prez\tt!ping\tt!pong\tt!google +une recherche\tt!play +le nom d'un fichier\n`;
    rep_help += `Morgpion est en train de chercher comment obtenir la liste des fichiers à jouer avec "t!play.`
    message.channel.send(rep_help);
  }

}



