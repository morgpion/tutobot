
const Command = require('./Command');

module.exports = class Pong extends Command {

  static match(message) {
    console.log('check pong');
    return message.content.startsWith('t!pong')
  }

  static action (message) {
    console.log('check ping');
    message.channel.send('t!ping');
  }

}
