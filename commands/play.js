
const Command = require('./Command');

module.exports = class Play extends Command {

  static match(message) {
    console.log('check play');
    return message.content.startsWith('t!play')
  }

  //  un seul titre disponible pour l'instant
  //  TODO : mettre d'autres titres, les modifier pour faciliter la lecture,
  //  faire une méthode pour lire les fichiers de musique
  static action (message) {
    //  debug :
    // console.log(GuildChannelManager);
    // console.log(message.guild.channels.cache.array());
    let voiceChannel = message.guild.channels.cache
      .filter((channel) => { return channel.type === 'voice' })
      .first()
    let args = message.content.split(' ');
    voiceChannel
      .join()
      //  connection de FFmpeg
      .then((connection) => {
        try {
          let stream = args[1];
          console.log(stream);
          //  méthode play() de connection
          connection.play(`./music/${stream}.mp3`)
        } catch(e) {
          message.reply("T'es sûr que c'est le bon titre ?");
        }
    })
  }

}
