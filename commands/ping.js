
//  import de la classe Command pour l'utiliser
const Command = require('./Command');

//  déclaration de la classe Ping, qui hérite de la classe Command
module.exports = class Ping extends Command {
//  méthodes héritées de Command

  //  implémentation de la commande pour appeler cette fonctionnalité
  static match(message) {
    //  pour vérifier l'ordre du parcours des classes, 
    //  peut servir à optimiser l'ordre sur index.js
    console.log('check ping');
    //  retourne le nom de la commande : méthode match('t!google')
    return message.content.startsWith('t!ping')
  }

  //  implémentation de la méthode action(message)
  static action (message) {
    //  répondre 'Pong' dans le channel
    message.reply('Pong');
  }

}
