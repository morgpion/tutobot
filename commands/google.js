
//  import de la classe Command pour l'utiliser
const Command = require('./Command');

//  déclaration de la classe Google, qui hérite de la classe Command
module.exports = class Google extends Command {
//  méthodes héritées de Command
  
  //  implémentation de la commande pour appeler cette fonctionnalité
  static match(message) {
    //  pour vérifier l'ordre du parcours des classes, 
    //  peut servir à optimiser l'ordre sur index.js
    console.log('check Google');
  //  retourne le nom de la commande : méthode match('t!google')
    return message.content.startsWith('t!google')
  }

  //  implémentation de la méthode action(message)
  static action (message) {
    //  spliter la commande avec un espace pour séparer les éléments
    //  stocker le résultat dans la variable args
    let args = message.content.split(' ');
    //  retirer le 1er élément de args
    args.shift();
    //  supprimer le contenu de message
    message.delete();
    //  affecter à message l'url de la recherche Google correspondant à la commande envoyée
    //  sous forme de la variable args en joignant les éléments par un espace
    message.reply('https://www.google.fr/#q='+args.join('%20'));
  }

}
