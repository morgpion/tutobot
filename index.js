
const Discord = require('discord.js');
//  commandes
const Google = require('./commands/google');
const Ping = require('./commands/ping');
const Pong = require('./commands/pong');
const Play = require('./commands/play');
const Prez = require('./commands/prez');
const Help = require('./commands/help');
// const Youtube = require('./commands/youtube');

//  exteernalisation du token dans les variables d'environnement
const dotenv = require('dotenv');
const result = dotenv.config();
const token = process.env.BOT_TOKEN;
// console.log(token);  //  pour vérifier


// il faut créer une instance de Client pour le bot
const bot = new Discord.Client();

bot.on('ready', () => {
  // setter l'image et l'activité à l'activation (permanent, inutile de le laiser une fois que c'est fait)
  // bot.user.setAvatar('./img/avatar.jpeg').then(()=> console.log('Avatar installé')).catch(console.error);
  // bot.user.setActivity('My garden\'s game').then(() => console.log('game ok')).catch(console.error);
  console.log('ready');
})

//  dire bienvenue à un nouvel utilisateur en message privé
//  ne marche pas si le bot a les droits d'admin
//  add member à guildMember (le serveur) en utilisaant ses méthodes
bot.on('guildMemberAdd', (member) => {
  //  créer un channel de message privé (les DM sont des channels "presque comme les autres")
  //  méthode createDM de GuildMember
  member.createDM().then((channel) => {
    //  méthode send() de GuildMember
    return channel.send(`Salut à toi ${member.displayName}`)
  }).catch(console.error)
})

//  parcourir les méthodes correspondant aux commandes de la classe Command
//  pour lancer celle appelée
bot.on('message', (message) => {
  let commandUsed = 
    Help.parse(message) ||
    Ping.parse(message) || 
    Pong.parse(message) || 
    Prez.parse(message) ||
    Play.parse(message) ||
    Google.parse(message);
})

//  connecter le bot à l'API avec le token comme équivalent de clé privée
bot.login(token);
